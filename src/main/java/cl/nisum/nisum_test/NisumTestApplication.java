package cl.nisum.nisum_test;

import cl.nisum.nisum_test.entities.HairColorEntity;
import cl.nisum.nisum_test.repositories.HairColorRepository;
import cl.nisum.nisum_test.services.impl.HairColorServiceImpl;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class NisumTestApplication {

    //@Autowired private HairColorRepository hairColorRepository;

    public static void main(String[] args) {
        SpringApplication.run(NisumTestApplication.class, args);
    }

    /*@Bean
    InitializingBean sendDatabase() {
        return () -> {
            hairColorRepository.save(new HairColorEntity(null, "Negro"));
            hairColorRepository.save(new HairColorEntity(null, "Rojo"));
            hairColorRepository.save(new HairColorEntity(null, "Amarillo"));
            hairColorRepository.save(new HairColorEntity(null, "Marrón"));
            hairColorRepository.save(new HairColorEntity(null, "Blanco"));
        };
    }*/
}

