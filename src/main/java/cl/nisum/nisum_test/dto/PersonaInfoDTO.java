package cl.nisum.nisum_test.dto;

import cl.nisum.nisum_test.entities.PersonaInfoEntity;

public class PersonaInfoDTO {
    private Integer id;
    private Integer age;
    private String firstName;
    private String lastName;
    private String address;
    private Integer phoneNumber;
    private String hairColor;

    public static PersonaInfoDTO copyFromEntity(PersonaInfoEntity entity) {
        PersonaInfoDTO dto = new PersonaInfoDTO();
        dto.setAge(entity.getAge());
        dto.setAddress(entity.getAddress());
        dto.setFirstName(entity.getFirstName());
        dto.setLastName(entity.getLastName());
        dto.setHairColor(entity.getHairColor().getName());
        dto.setId(entity.getId());
        dto.setPhoneNumber(entity.getPhoneNumber());
        return dto;
    }

    public PersonaInfoDTO() {
    }

    public PersonaInfoDTO(Integer id, String firstName, String lastName, String address,
                          Integer phoneNumber, String hairColor) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.hairColor = hairColor;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(Integer phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getHairColor() {
        return hairColor;
    }

    public void setHairColor(String hairColor) {
        this.hairColor = hairColor;
    }
}
