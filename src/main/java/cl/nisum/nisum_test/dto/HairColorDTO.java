package cl.nisum.nisum_test.dto;

import cl.nisum.nisum_test.entities.HairColorEntity;

public class HairColorDTO {
    private Integer id;
    private String name;

    public static HairColorDTO copyFromEntity(HairColorEntity entity) {
        HairColorDTO dto = new HairColorDTO();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        return dto;
    }

    public HairColorDTO() {
    }

    public HairColorDTO(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
