package cl.nisum.nisum_test.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.Calendar;

public class ApiError {
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private Calendar timestamp;
    private int status;
    private String message;
    private String method;
    private String path;

    private String debugMessage;


    public ApiError(String message) {
        this.message = message;
        timestamp = Calendar.getInstance();
    }

    public ApiError(HttpStatus httpStatus, String message) {
        this(message);
        this.status = httpStatus.value();
    }

    public ApiError(WebRequest request, HttpStatus httpStatus, String message) {
        this(httpStatus, message);
        this.preparePath(request);
    }

    public Calendar getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Calendar timestamp) {
        this.timestamp = timestamp;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getDebugMessage() {
        return debugMessage;
    }

    public void setDebugMessage(String debugMessage) {
        this.debugMessage = debugMessage;
    }

    public HttpStatus getHttpStatus() {
        return HttpStatus.valueOf(this.status);
    }

    private void preparePath(WebRequest request) {
        if (request instanceof ServletWebRequest) {
            this.preparePath(((ServletWebRequest)request).getRequest());
        }
        else {
            this.path = request.getDescription(false);
            if (this.path != null && this.path.startsWith("uri=")) {
                this.path = this.path.substring(4);
            }
        }
    }

    private void preparePath(HttpServletRequest request) {
        this.method = request.getMethod();
        this.path = request.getRequestURI();
    }

}
