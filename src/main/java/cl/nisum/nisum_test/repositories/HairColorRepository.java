package cl.nisum.nisum_test.repositories;

import cl.nisum.nisum_test.entities.HairColorEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HairColorRepository extends JpaRepository<HairColorEntity, Integer> {
    HairColorEntity findByNameEquals(String name);
}
