package cl.nisum.nisum_test.repositories;

import cl.nisum.nisum_test.entities.PersonaInfoEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PersonaInfoRepository extends JpaRepository<PersonaInfoEntity, Integer> {
    List<PersonaInfoEntity> findAllByHairColor_Id(Integer hairColor_id);
    List<PersonaInfoEntity> findAllByAgeBetween(Integer age, Integer age2);
}
