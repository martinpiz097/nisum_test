package cl.nisum.nisum_test.data;

import cl.nisum.nisum_test.entities.HairColorEntity;
import cl.nisum.nisum_test.repositories.HairColorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class DataLoader implements ApplicationRunner {

    @Autowired private HairColorRepository hairColorRepository;

    public void run(ApplicationArguments args) {
        hairColorRepository.save(new HairColorEntity(null, "Negro"));
        hairColorRepository.save(new HairColorEntity(null, "Rojo"));
        hairColorRepository.save(new HairColorEntity(null, "Amarillo"));
        hairColorRepository.save(new HairColorEntity(null, "Marrón"));
        hairColorRepository.save(new HairColorEntity(null, "Blanco"));
    }
}