package cl.nisum.nisum_test.services;

import cl.nisum.nisum_test.dto.HairColorDTO;
import cl.nisum.nisum_test.exception.EntityNotFoundException;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface HairColorService {
    ResponseEntity<List<HairColorDTO>> getAllHairColors();
    ResponseEntity<HairColorDTO> getHairColorById(Integer id) throws EntityNotFoundException;
    ResponseEntity<HairColorDTO> getHairColorByName(String name) throws EntityNotFoundException;
}
