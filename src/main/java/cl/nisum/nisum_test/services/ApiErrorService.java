package cl.nisum.nisum_test.services;

import cl.nisum.nisum_test.dto.ApiError;

public interface ApiErrorService {
    void resolveMessage(ApiError apiError);
}
