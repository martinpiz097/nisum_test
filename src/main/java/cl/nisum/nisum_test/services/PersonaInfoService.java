package cl.nisum.nisum_test.services;

import cl.nisum.nisum_test.dto.PersonaInfoDTO;
import cl.nisum.nisum_test.exception.EntityNotFoundException;
import cl.nisum.nisum_test.exception.InvalidFormatException;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface PersonaInfoService {
    ResponseEntity addPerson(PersonaInfoDTO personaInfo) throws EntityNotFoundException, InvalidFormatException;
    ResponseEntity<List<PersonaInfoDTO>> getAllPersons();
    ResponseEntity<PersonaInfoDTO> getPersonById(Integer personId) throws EntityNotFoundException;
    ResponseEntity<List<PersonaInfoDTO>> getAllByAgeRange(Integer from, Integer to);
    ResponseEntity removePersonById(Integer personId) throws EntityNotFoundException;
    ResponseEntity<Boolean> updatePerson(PersonaInfoDTO personaInfo) throws EntityNotFoundException, InvalidFormatException;
}
