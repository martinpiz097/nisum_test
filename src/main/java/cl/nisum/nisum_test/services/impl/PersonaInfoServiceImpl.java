package cl.nisum.nisum_test.services.impl;

import cl.nisum.nisum_test.dto.PersonaInfoDTO;
import cl.nisum.nisum_test.entities.HairColorEntity;
import cl.nisum.nisum_test.entities.PersonaInfoEntity;
import cl.nisum.nisum_test.exception.EntityNotFoundException;
import cl.nisum.nisum_test.exception.InvalidFormatException;
import cl.nisum.nisum_test.repositories.HairColorRepository;
import cl.nisum.nisum_test.repositories.PersonaInfoRepository;
import cl.nisum.nisum_test.services.PersonaInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static cl.nisum.nisum_test.system.StringUtils.hasNumbers;

@Service
@Transactional
public class PersonaInfoServiceImpl implements PersonaInfoService {

    @Autowired private PersonaInfoRepository personaInfoRepository;
    @Autowired private HairColorRepository hairColorRepository;

    @Override
    public ResponseEntity addPerson(PersonaInfoDTO personaInfo) throws EntityNotFoundException, InvalidFormatException {
        PersonaInfoEntity entity = new PersonaInfoEntity();
        HairColorEntity color = hairColorRepository.findByNameEquals(personaInfo.getHairColor());
        if (color == null) {
            throw new EntityNotFoundException("Color de cabello no encontrado");
        }

        else {
            if (hasNumbers(personaInfo.getFirstName())
                     || hasNumbers(personaInfo.getLastName())) {
                if (hasNumbers(personaInfo.getFirstName())
                        && hasNumbers(personaInfo.getLastName()))
                    throw new InvalidFormatException("El nombre y el apellido no pueden contener números");

                else if (hasNumbers(personaInfo.getFirstName())) {
                    throw new InvalidFormatException("El nombre no puede contener números");
                }
                else {
                    throw new InvalidFormatException("El apellido no puede contener números");
                }
            }

            if (!personaInfo.getAddress().trim().contains(" ")) {
                throw new InvalidFormatException("La direccion debe contener espacios");
            }

            entity.setAddress(personaInfo.getAddress());
            entity.setAge(personaInfo.getAge());
            entity.setFirstName(personaInfo.getFirstName());
            entity.setLastName(personaInfo.getLastName());
            entity.setPhoneNumber(personaInfo.getPhoneNumber());
            entity.setHairColor(color);
            personaInfoRepository.save(entity);
            return ResponseEntity.status(HttpStatus.CREATED).build();
        }
    }

    @Override
    public ResponseEntity<List<PersonaInfoDTO>> getAllPersons() {
        List<PersonaInfoEntity> listEntities = personaInfoRepository.findAll();
        List<PersonaInfoDTO> listDtos = new ArrayList<>();

        listEntities.forEach(entity -> {
            listDtos.add(PersonaInfoDTO.copyFromEntity(entity));
        });

        return listDtos.isEmpty()
                ? ResponseEntity.noContent().build()
                : ResponseEntity.ok(listDtos);
    }

    @Override
    public ResponseEntity<PersonaInfoDTO> getPersonById(Integer personId) throws EntityNotFoundException {
        PersonaInfoEntity entity = personaInfoRepository.findById(personId)
                .orElse(null);
        if (entity == null) {
            throw new EntityNotFoundException("La persona no ha sido encontrada");
        }
        return ResponseEntity.ok(PersonaInfoDTO.copyFromEntity(entity));
    }

    @Override
    public ResponseEntity<List<PersonaInfoDTO>> getAllByAgeRange(Integer from, Integer to) {
        List<PersonaInfoEntity> listEntities = personaInfoRepository.findAllByAgeBetween(from, to);
        List<PersonaInfoDTO> listDtos = new ArrayList<>();

        listEntities.forEach(entity ->
            listDtos.add(PersonaInfoDTO.copyFromEntity(entity))
        );

        return listDtos.isEmpty()
                ? ResponseEntity.noContent().build()
                : ResponseEntity.ok(listDtos);
    }

    @Override
    public ResponseEntity removePersonById(Integer personId) throws EntityNotFoundException {
        PersonaInfoEntity entity = personaInfoRepository.findById(personId)
                .orElse(null);

        if (entity == null) {
            throw new EntityNotFoundException("La persona no ha sido encontrada");
        }
        else {
            personaInfoRepository.delete(entity);
            return ResponseEntity.ok().build();
        }
    }

    @Override
    public ResponseEntity<Boolean> updatePerson(PersonaInfoDTO personaInfo) throws EntityNotFoundException, InvalidFormatException {
        PersonaInfoEntity entity = personaInfoRepository.findById(personaInfo.getId())
                .orElse(null);

        if (entity == null) {
            throw new EntityNotFoundException("La persona no ha sido encontrada");
        }

        else {
            HairColorEntity color = hairColorRepository.findByNameEquals(personaInfo.getHairColor());
            if (color == null) {
                throw new EntityNotFoundException("Color de cabello no encontrado");
            }

            if (hasNumbers(personaInfo.getFirstName())
                    || hasNumbers(personaInfo.getLastName())) {
                if (hasNumbers(personaInfo.getFirstName())
                        && hasNumbers(personaInfo.getLastName()))
                    throw new InvalidFormatException("El nombre y el apellido no pueden contener números");

                else if (hasNumbers(personaInfo.getFirstName())) {
                    throw new InvalidFormatException("El nombre no puede contener números");
                }
                else {
                    throw new InvalidFormatException("El apellido no puede contener números");
                }
            }

            if (!personaInfo.getAddress().trim().contains(" ")) {
                throw new InvalidFormatException("La direccion debe contener espacios");
            }

            entity.setAddress(personaInfo.getAddress());
            entity.setAge(personaInfo.getAge());
            entity.setFirstName(personaInfo.getFirstName());
            entity.setLastName(personaInfo.getLastName());
            entity.setPhoneNumber(personaInfo.getPhoneNumber());
            entity.setHairColor(color);
            personaInfoRepository.save(entity);
            return ResponseEntity.ok().build();
        }
    }
}
