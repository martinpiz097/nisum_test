package cl.nisum.nisum_test.services.impl;

import cl.nisum.nisum_test.dto.ApiError;
import cl.nisum.nisum_test.services.ApiErrorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import java.util.Locale;

@Service
public class ApiErrorServiceImpl implements ApiErrorService {
    @Autowired
    private MessageSource messageSource;

    @Override
    public void resolveMessage(ApiError apiError) {
        String message = apiError.getMessage();
        if (message != null) {
            Locale local = LocaleContextHolder.getLocale();
            message = this.messageSource.getMessage(message, null, local);
            apiError.setMessage(message);
        }
    }
}
