package cl.nisum.nisum_test.services.impl;

import cl.nisum.nisum_test.dto.HairColorDTO;
import cl.nisum.nisum_test.entities.HairColorEntity;
import cl.nisum.nisum_test.exception.EntityNotFoundException;
import cl.nisum.nisum_test.repositories.HairColorRepository;
import cl.nisum.nisum_test.services.HairColorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class HairColorServiceImpl implements HairColorService {

    @Autowired private HairColorRepository hairColorRepository;

    @Override
    public ResponseEntity<List<HairColorDTO>> getAllHairColors() {
        List<HairColorEntity> listEntities = hairColorRepository.findAll();
        List<HairColorDTO> listDtos = new ArrayList<>();

        listEntities.forEach(entity -> listDtos.add(HairColorDTO.copyFromEntity(entity)));

        return listDtos.isEmpty()
                ? ResponseEntity.noContent().build()
                : ResponseEntity.ok(listDtos);
    }

    @Override
    public ResponseEntity<HairColorDTO> getHairColorById(Integer id) throws EntityNotFoundException {
        HairColorEntity entity = hairColorRepository.findById(id)
                .orElse(null);

        if (entity == null) {
            throw new EntityNotFoundException("El color de cabello no existe");
        }

        return ResponseEntity.ok(HairColorDTO.copyFromEntity(entity));
    }

    @Override
    public ResponseEntity<HairColorDTO> getHairColorByName(String name) throws EntityNotFoundException {
        HairColorEntity entity = hairColorRepository.findByNameEquals(name);
        if (entity == null) {
            throw new EntityNotFoundException("El color de cabello no existe");
        }
        return ResponseEntity.ok(HairColorDTO.copyFromEntity(entity));
    }
}
