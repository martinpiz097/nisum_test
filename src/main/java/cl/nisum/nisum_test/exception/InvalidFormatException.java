package cl.nisum.nisum_test.exception;

import cl.nisum.nisum_test.dto.ApiError;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class InvalidFormatException extends ApiException {
    public InvalidFormatException(String message) {
        super(new ApiError(HttpStatus.BAD_REQUEST, message));
    }
}
