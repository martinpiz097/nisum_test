package cl.nisum.nisum_test.exception;

import cl.nisum.nisum_test.dto.ApiError;

public class ApiException extends Exception {

    private ApiError apiError;

    public ApiException(ApiError apiError) {
        super(apiError.getMessage());
        this.apiError = apiError;
    }

    public ApiError getError() {
        return apiError;
    }
}
