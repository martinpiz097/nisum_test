package cl.nisum.nisum_test.exception;

import cl.nisum.nisum_test.dto.ApiError;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class EntityNotFoundException extends ApiException {
    public EntityNotFoundException() {
        this("EntityNotFoundException");
    }

    public EntityNotFoundException(String message) {
        super(new ApiError(HttpStatus.NOT_FOUND, message));
    }
}
