package cl.nisum.nisum_test.controllers;

import cl.nisum.nisum_test.dto.HairColorDTO;
import cl.nisum.nisum_test.exception.EntityNotFoundException;
import cl.nisum.nisum_test.services.HairColorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/colors")
public class ApiHairColorController {

    @Autowired private HairColorService hairColorService;

    @GetMapping("/all")
    @ResponseBody
    public ResponseEntity<List<HairColorDTO>> getAllHairColors() {
        return hairColorService.getAllHairColors();
    }

    @GetMapping("/id/{id}")
    @ResponseBody
    public ResponseEntity<HairColorDTO> getHairColorById(@PathVariable Integer id) throws EntityNotFoundException {
        return hairColorService.getHairColorById(id);
    }

    @GetMapping("/name/{name}")
    @ResponseBody
    public ResponseEntity<HairColorDTO> getHairColorByName(@PathVariable String name) throws EntityNotFoundException {
        return hairColorService.getHairColorByName(name);
    }
}
