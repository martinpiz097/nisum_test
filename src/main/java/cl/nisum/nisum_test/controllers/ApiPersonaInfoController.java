package cl.nisum.nisum_test.controllers;

import cl.nisum.nisum_test.dto.PersonaInfoDTO;
import cl.nisum.nisum_test.exception.EntityNotFoundException;
import cl.nisum.nisum_test.exception.InvalidFormatException;
import cl.nisum.nisum_test.services.PersonaInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/persons", produces = MediaType.APPLICATION_JSON_VALUE)
public class ApiPersonaInfoController {

    @Autowired private PersonaInfoService personaInfoService;

    @PostMapping
    @ResponseBody
    public ResponseEntity addPerson(@RequestBody PersonaInfoDTO personaInfo) throws InvalidFormatException, EntityNotFoundException {
        return personaInfoService.addPerson(personaInfo);
    }

    @GetMapping("/all")
    @ResponseBody
    public ResponseEntity<List<PersonaInfoDTO>> getAllPersons() {
        return personaInfoService.getAllPersons();
    }

    @GetMapping("/id/{personId}")
    public ResponseEntity<PersonaInfoDTO> getPersonById(@PathVariable Integer personId) throws EntityNotFoundException {
        return personaInfoService.getPersonById(personId);
    }

    @GetMapping("/range")
    public ResponseEntity<List<PersonaInfoDTO>> getAllByAgeRange(
            @RequestParam("from") Integer from,
            @RequestParam("to") Integer to) {
        return personaInfoService.getAllByAgeRange(from, to);
    }

    @DeleteMapping("/id/{personId}")
    @ResponseBody
    public ResponseEntity removePersonById(@PathVariable Integer personId) throws EntityNotFoundException {
        return personaInfoService.removePersonById(personId);
    }

    @PutMapping
    @ResponseBody
    public ResponseEntity<Boolean> updatePerson(@RequestBody PersonaInfoDTO personaInfo) throws EntityNotFoundException, InvalidFormatException {
        return personaInfoService.updatePerson(personaInfo);
    }
}
