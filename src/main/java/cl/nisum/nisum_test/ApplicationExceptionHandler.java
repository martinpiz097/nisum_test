package cl.nisum.nisum_test;

import cl.nisum.nisum_test.dto.ApiError;
import cl.nisum.nisum_test.exception.ApiException;
import cl.nisum.nisum_test.services.ApiErrorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class ApplicationExceptionHandler extends ResponseEntityExceptionHandler {
    @Autowired
    private MessageSource messageSource;

    @Autowired
    private ApiErrorService apiErrorService;

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public final ResponseEntity<Object> defaultErrorHandler(Exception ex,
                                                            WebRequest request) throws Exception {
        if (AnnotationUtils.findAnnotation(ex.getClass(),
                ResponseStatus.class) != null) {
            throw ex;
        }
        ex.printStackTrace();
        return this.buildResponseEntityDefault(ex, request);
    }

    @ExceptionHandler(value = ApiException.class)
    @ResponseBody
    public ResponseEntity apiErrorHandler(Exception ex, WebRequest request) {
        return this.handleApiException((ApiException)ex, request);
    }

    private ResponseEntity handleApiException(ApiException ex, WebRequest request) {
        return this.resolveMessage(ex.getError(), request);
    }

    private ResponseEntity<Object> buildResponseEntityDefault(Exception ex, WebRequest request) {
        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        ApiError apiError = new ApiError(request, status, "Exception");
        apiError.setDebugMessage(ex.getClass().getSimpleName());
        return this.resolveMessage(apiError, request);
    }

    private ResponseEntity resolveMessage(ApiError apiError, WebRequest request) {
        this.apiErrorService.resolveMessage(apiError);
        return new ResponseEntity<>(apiError, apiError.getHttpStatus());
    }
}
