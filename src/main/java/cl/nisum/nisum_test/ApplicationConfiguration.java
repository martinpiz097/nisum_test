package cl.nisum.nisum_test;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@ComponentScan(basePackages = {"cl.nisum.nisum_test.services", "cl.nisum.nisum_test.data"})
@EntityScan(basePackages = "cl.nisum.nisum_test.entities")
@EnableJpaRepositories(basePackages = "cl.nisum.nisum_test.repositories")
@EnableJpaAuditing
@EnableTransactionManagement
public class ApplicationConfiguration {

}
