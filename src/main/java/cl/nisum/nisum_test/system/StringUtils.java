package cl.nisum.nisum_test.system;

public class StringUtils {
    public static boolean hasNumbers(String str) {
        final char[] chars = str.toCharArray();
        char c;
        for (int i = 0; i < chars.length; i++) {
            c = chars[i];
            if (c > 47 && c < 58) {
                return true;
            }
        }
        return false;
    }
}
